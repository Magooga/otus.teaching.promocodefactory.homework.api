﻿using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Otus.Teaching.PromoCodeFactory.WebHost.Protos;
using System.Threading.Tasks;

using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Linq;
using System;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using static Google.Rpc.Context.AttributeContext.Types;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Sevices
{


    public class CustomerApiService : CustomerNativeGrpc.CustomerNativeGrpcBase
    {

        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomerApiService(IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        // отправляем список
        public override async Task<ListReply> GetCustomers(Empty request, ServerCallContext context)
        { 
            var listReply = new ListReply();
            // преобразуем каждый объект из списка users в объект UserReply

            var customers = await _customerRepository.GetAllAsync();


            var customerList = customers.Select(x => new CustomerShortReply() 
            {
                Id = new UUID() { Value = x.Id.ToString() },
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();

            listReply.Customers.AddRange(customerList);

            return listReply;
        }

        public override async Task<CustomerReply> GetCustomer(GetCustomerRequest request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(new Guid(request.Id.Value));

            // если пользователь не найден, генерируем исключение
            if (customer == null)
            {
                throw new RpcException(new Status(StatusCode.NotFound, "User not found"));
            }

            var prefList = customer.Preferences.Select(x => new PreferenceReply()
            {
                Id = new UUID() { Value = x.PreferenceId.ToString() },
                Name = x.Preference.Name
            }).ToList();


            CustomerReply customerReply = new CustomerReply() { 
                Id = new UUID() { Value = customer.Id.ToString() },
                Email = customer.Email,
                FirstName = customer.FirstName,
            };

            customerReply.Preferences.AddRange(prefList);

            var response = new CustomerResponse(customer);

            return customerReply;
        }

        public override async Task<CreateCustomerReply> CreateCustomer(CreateCustomerRequest request, ServerCallContext context)
        {
            //Получаем предпочтения из бд и сохраняем большой объект
            var preferences = await _preferenceRepository
                .GetRangeByIdsAsync(request.PreferenceIds.Select(x => new Guid(x)).ToList());

            Customer customer = CustomerMapper.MapFromModel(new CreateOrEditCustomerRequest()
                {
                    FirstName = request.FirstName,
                    LastName = request.LastName,
                    Email = request.Email,
                }, 
            preferences);

            await _customerRepository.AddAsync(customer);

            return new CreateCustomerReply() 
            { 
                Id = new UUID { Value = customer.Id.ToString() } 
            };
        }


        public override async Task<Empty> EditCustomers(EditCustomerRequest request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(new Guid(request.Id.Value));

            // если пользователь не найден, генерируем исключение
            if (customer == null)
            {
                throw new RpcException(new Status(StatusCode.NotFound, "User not found"));
            }

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds.Select(x => new Guid(x)).ToList());

            CustomerMapper.MapFromModel(new CreateOrEditCustomerRequest()
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
            },
            preferences, customer);

            await _customerRepository.UpdateAsync(customer);

            return new Empty();
        }

        public override async Task<Empty> DeleteCustomer(DeleteCustomerRequest request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(new Guid(request.Id.Value));

            // если пользователь не найден, генерируем исключение
            if (customer == null)
            {
                throw new RpcException(new Status(StatusCode.NotFound, "User not found"));
            }

            await _customerRepository.DeleteAsync(customer);

            return new Empty();
        }
    }
}
